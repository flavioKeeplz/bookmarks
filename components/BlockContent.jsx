import React from 'react'
import PropTypes from 'prop-types'

const BlockContent = ({ bookmarks, removeBookmark, removeBookmarkTag }) => (
    <div className="flex flex-col mt-8">
        {bookmarks.map(({ title, link, tags, id }) => (
            <div className={`flex-1 flex justify-between py-4${bookmarks.length > id ? ' border-b border-grey-light' : ''}`} key={id}>
                <div className="flex flex-col">
                    <h3 className="font-normal text-grey-darkest">{title}</h3>
                    <a href={`http://${link}`} className="no-underline hover:underline text-blue-light text-sm my-2">{link}</a>
                    <div className="flex -mx-2 text-xs">
                        {tags.map((tag, index) => (
                            <span className="mx-2 bg-blue-dark uppercase text-xs font-bold text-white p-1 px-2 rounded-sm flex items-center" key={index}>
                                {tag}
                                <button className="focus:outline-none ml-3" onClick={() => removeBookmarkTag(id, index)}>
                                    <img width="7" src="/static/images/X.png" alt="delete"/>
                                </button>
                            </span>
                        ))}
                    </div>
                </div>
                <div className="flex-1 flex justify-end items-center">
                    <button className="focus:outline-none ml-3" onClick={() => removeBookmark(id)}>
                        <img src="/static/images/Trash.png" alt="trash"/>
                        <span className="text-grey uppercase text-xs font-bold ml-3 leading-loose align-text-bottom">delete</span>
                    </button>
                </div>
            </div>
        ))}
    </div>
)

BlockContent.propTypes = {
    bookmarks: PropTypes.arrayOf(
        PropTypes.shape({
        id: PropTypes.number.isRequired,
        title: PropTypes.string.isRequired,
        link: PropTypes.string.isRequired,
        tags: PropTypes.arrayOf(PropTypes.string)
        }).isRequired
    ).isRequired,
    removeBookmark: PropTypes.func.isRequired
}

export default BlockContent
