import React, { Component } from 'react'
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types'

import Input from './Input';

export default class BlockHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            filter: '',
            title: '',
            link: '',
            tags: '',
            formTags: [],
            showFilterByTag: false,
            showBookmarkForm: true,
            bookmarkIcon: 'azul',
            filterByTagIcon: 'cinza'
        }

        this.handleBookmarkForm = this.handleBookmarkForm.bind(this)
        this.handleFilterByTag = this.handleFilterByTag.bind(this)
        this.handleTags = this.handleTags.bind(this)
    }

    handleFilterByTag() {
        this.setState({
            showFilterByTag: true,
            showBookmarkForm: false,
            bookmarkIcon: 'cinza',
            filterByTagIcon: 'azul'
        })
    }

    handleBookmarkForm(e) {
        e.preventDefault()

        if(this.state.showBookmarkForm) {
            if(
                this.state.title.length > 0 &&
                this.state.link.length > 0 &&
                this.state.formTags.length > 0
            ) {
                let bookmark = {
                    title: this.state.title,
                    link: this.state.link,
                    tags: this.state.formTags
                }
                this.props.addBookmark(bookmark)
            }
        }

        this.setState({
            showFilterByTag: false,
            showBookmarkForm: true,
            bookmarkIcon: 'azul',
            filterByTagIcon: 'cinza',
            title: '',
            link: '',
            tags: '',
            formTags: []
        })
    }

    handleTags(e) {
        let formTags = this.state.formTags,
            tags = e.target.value,
            value = e.target.value

        if(value.slice(-1) === ' ') {
            formTags.push(e.target.value.trim())
            tags = ''
        }
        this.setState({
            formTags,
            tags
        })
    }

    render() {
        const {
            showBookmarkForm,
            showFilterByTag,
            title,
            link,
            tags,
            formTags
        } = this.state

        return (
            <div className="flex flex-row -mx-2">
                <button className="focus:outline-none mx-2" onClick={e => this.handleFilterByTag(e)}>
                    <img width="30" src={`/static/images/Lupa_${this.state.filterByTagIcon}.png`} alt=""/>
                </button>
                <button className="focus:outline-none mx-2" onClick={e => this.handleBookmarkForm(e)}>
                    <img width="30" src={`/static/images/Adicionar_${this.state.bookmarkIcon}.png`} alt=""/>
                </button>
                <div className="flex-1">
                    <CSSTransition unmountOnExit timeout={200} classNames="fade" in={showBookmarkForm} className={showBookmarkForm ? 'w-full flex-1 flex' : 'hidden'}>
                        <form className="flex flex-row mx-2 flex-1 flex-between" onSubmit={e => this.handleBookmarkForm(e)}>
                            <div className="mx-2 w-1/3">
                                <Input placeholder="Title" value={title} onChange={e => this.setState({title: e.target.value})}/>
                            </div>
                            <div className="mx-2 w-1/3">
                                <Input placeholder="Link" value={link} onChange={e => this.setState({link: e.target.value})}/>
                            </div>
                            <div className="mx-2 w-1/3">
                                <Input placeholder="Tags" tags={formTags} value={tags} onChange={e => this.handleTags(e)}/>
                            </div>
                        </form>
                    </CSSTransition>

                    <CSSTransition unmountOnExit timeout={200} classNames="fade" in={showFilterByTag}  className={showFilterByTag ? 'w-full flex-1 flex' : 'hidden'}>
                    <div className="flex flex-row mx-2 flex-1 flex-between">
                        <div className="mx-2 w-full">
                            <Input placeholder="Tag" value={this.state.filter} onChange={e => {
                                this.setState({filter: e.target.value})
                                this.props.filterBookmarkByTag(e.target.value)
                            }} />
                        </div>
                    </div>
                    </CSSTransition>
                </div>
            </div>
        )
    }
}

BlockHeader.prototypes = {
    addBookmark: PropTypes.func.isRequired,
    filterBookmarkByTag: PropTypes.func.isRequired
}