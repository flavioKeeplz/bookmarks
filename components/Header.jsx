import React, { Component } from 'react'

export default class Header extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <header className="flex flex-between items-center w-full py-4">
                <span className="text-grey-dark font-bold text-2xl">Bookmarks Example</span>
            </header>
        )
    }
}