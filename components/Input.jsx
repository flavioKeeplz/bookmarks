import React from 'react'
import PropTypes from 'prop-types'

const Input = ({ placeholder, value, onChange, tags }) => (
    <div className="border border-grey-light w-full rounded text-grey-dark flex flex-row flex-wrap px-4">
        {typeof tags === 'object' ? tags.map((tag, index) => (
                <span className="m-1 bg-blue-dark uppercase text-xs font-bold text-white p-1 px-2 rounded-sm flex items-center h-auto" key={index}>
                    {tag}
                    <button className="focus:outline-none ml-3" onClick={() => delete tags[index]}>
                        <img width="7" src="/static/images/X.png" alt="delete"/>
                    </button>
                </span>
            )) : null
        }
        <input
            id={placeholder}
            type="text"
            placeholder={placeholder}
            value={value}
            onChange={onChange}
            className="flex-1 py-3 text-grey-dark focus:outline-none appearance-none bg-transparent"
        />
    </div>
)

Input.prototypes ={
    placeholder: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    tags: PropTypes.arrayOf(PropTypes.string)
}

export default Input