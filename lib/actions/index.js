import { BOOKMARK_ADD, BOOKMARK_REMOVE, BOOKMARK_REMOVE_TAG, BOOKMARK_FILTER_BY_TAG } from './types'

export const addBookmark = bookmark => ({
    type: BOOKMARK_ADD,
    bookmark
})

export const removeBookmark = id => ({
    type: BOOKMARK_REMOVE,
    id
})

export const removeBookmarkTag = (id, tag) => ({
    type: BOOKMARK_REMOVE_TAG,
    id,
    tag
})

export const filterBookmarkByTag = (tag) => ({
    type: BOOKMARK_FILTER_BY_TAG,
    tag
})

export const getBookmarks = (bookmarks, filter) => {
    return filter.length > 0 ? bookmarks.filter(bookmark => {
        return bookmark.tags.filter(tag => {
            tag = clean(tag.toLowerCase())
            filter = clean(filter.toLowerCase())
            
            return tag.includes(filter)
        }).length > 0
    }) : bookmarks
}

const clean = (string) => {
    var mapaAcentosHex 	= {
        a : /[\xE0-\xE6]/g,
        A : /[\xC0-\xC6]/g,
        e : /[\xE8-\xEB]/g,
        E : /[\xC8-\xCB]/g,
        i : /[\xEC-\xEF]/g,
        I : /[\xCC-\xCF]/g,
        o : /[\xF2-\xF6]/g,
        O : /[\xD2-\xD6]/g,
        u : /[\xF9-\xFC]/g,
        U : /[\xD9-\xDC]/g,
        c : /\xE7/g,
        C : /\xC7/g,
        n : /\xF1/g,
        N : /\xD1/g
    };

    for ( var letra in mapaAcentosHex ) {
        var expressaoRegular = mapaAcentosHex[letra];
        string = string.replace( expressaoRegular, letra );
    }

    return string
} 