import { connect } from 'react-redux';
import { removeBookmark, removeBookmarkTag, getBookmarks } from '../actions'
import BlockContent from '../../components/BlockContent'

const mapStateToProps = state => ({
    bookmarks: getBookmarks(state.bookmarks, state.filterByTags)
})

const mapDispatchToProps = dispatch => ({
    removeBookmark: id => dispatch(removeBookmark(id)),
    removeBookmarkTag: (id, tag) => dispatch(removeBookmarkTag(id, tag))
})

export default connect(mapStateToProps, mapDispatchToProps)(BlockContent)