import { connect } from 'react-redux';
import { addBookmark, filterBookmarkByTag } from "../actions";
import BlockHeader from '../../components/BlockHeader'

const mapDispatchToProps = dispatch => ({
    addBookmark: bookmark => dispatch(addBookmark(bookmark)),
    filterBookmarkByTag: tag => dispatch(filterBookmarkByTag(tag))
})

export default connect(null, mapDispatchToProps)(BlockHeader)