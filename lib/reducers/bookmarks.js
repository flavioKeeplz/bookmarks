import { BOOKMARK_ADD, BOOKMARK_REMOVE, BOOKMARK_REMOVE_TAG, BOOKMARK_FILTER_BY_TAG } from '../actions/types'

let initialState = [
    {
        id: 1,
        title: 'Crédito',
        link: 'www.beblue.com.br',
        tags: ['crédito']
    },
    {
        id: 2,
        title: 'Wallet',
        link: 'www.beblue.com.br/wallet',
        tags: ['wallet']
    },
    {
        id: 3,
        title: 'Credenciamento',
        link: 'www.beblue.com.br/credenciamento',
        tags: ['wallet']
    },
    {
        id: 4,
        title: 'Seu dinheiro de volta',
        link: 'www.beblue.com.br',
        tags: ['beblue', 'cashback', 'saldo']
    }
]

const bookmarks = (state = initialState, action) => {
    switch (action.type) {
        case BOOKMARK_ADD:
            return [
                ...state,
                {
                    id: state.length + 1,
                    title: action.bookmark.title,
                    link: action.bookmark.link,
                    tags: action.bookmark.tags
                }
            ]
        case BOOKMARK_REMOVE:
            return state.filter(bookmark => bookmark.id !== action.id)
        case BOOKMARK_REMOVE_TAG:
            return state.filter(bookmark => {
                if(bookmark.id === action.id) {
                    delete bookmark.tags[action.tag]
                }

                return true
            })
        default:
            return state
    }
  }
  
  export default bookmarks