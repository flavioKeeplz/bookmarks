import { combineReducers } from 'redux'
import bookmarks from './bookmarks'
import filterByTags from './filterByTags'

export default combineReducers({
    bookmarks,
    filterByTags
})