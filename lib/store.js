import { createStore } from 'redux'
import rootReducer from './reducers'

export function initializeStore (initialState) {
  return createStore(
    rootReducer,
    initialState
  )
}