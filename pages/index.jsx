import React from 'react'
import Header from '../components/Header';
import HeaderActions from '../lib/containers/HeaderActions';
import Block from '../lib/containers/Block';

export default class Index extends React.Component {
    render() {
        return (
            <div className="bg-grey-lighter min-h-screen w-full">
                <div className="container mx-auto">
                    <Header className="mb-8" />
                    <div className="bg-white rounded-lg p-6">
                        <HeaderActions />
                        <Block />
                    </div>
                </div>
            </div>
        )
    }
}